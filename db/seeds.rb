# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

arr = ["Free Large R&Bs Special Pie", "R&Bs Hat/T-shirt"]

11.times do |n|
	Product.create(title: arr.shuffle.first, desc: arr.shuffle.first, quantity: n*33, active: true)
end

######################### Coupons #########################

coupons = {
	coupons: [
		{
			title: "Pizza&Breadsticks",
			price: 18.99,
			desc: "Large 16” 12-cut 1-Topping Pizza & 1 Order of Breadsticks"
		},
		{
			title: "Pizza & Wings",
			price: 23.99,
			desc: "Large 16” 12-Cut 1-Topping Pizza & Dozen Wings"
		},
		{
			title: "Pizza&Hoagies",
			price: 23.99,
			desc: "Large 16” 12-cut 1-Topping Pizza & Whole Italian Hoagie"
		},
		{
			title: "Wings & Hoagies",
			price: 19.99,
			desc: "One Dozen Wings and Any Whole 16” Hoagie"
		},
		{
			title: "GyroSpecial",
			price: 10.99,
			desc:"1 Gyro, French Fries and 1 Can of Soda"
		},
		{
			title: "Family Special",
			price: 33.99,
			desc: "Large 16” 12 Cut 1-Topping Special, Any Whole 16” Hoagie Dozen Wings & 2 Liter of Soda"
		},
		{
			title: "Pizza Special",
			extras: [
				{desc: "Large 16” 12 Cut 1-Topping Pizza", price: 12.99},
				{desc: "X-Large 18” 16 Cut 1-Topping Pizza", price: 13.99}
			]
		},
		{
			title: "Two Pizza Specials",
			extras: [
				{desc: "2 Small 10” 4 Cut 1-Topping Pizzas", price: 18.99},
				{desc: "2 Medium 14” 8 Cut 1-Topping Pizzas", price: 23.99},
				{desc: "2 Large 16” 12 Cut 1-Topping Pizzas", price: 25.99},
				{desc: "2 X-Large 18” 16 Cut 1-Topping Pizzas", price: 27.99}
			]
		},
		{
			title: "Wings Special",
			extras: [
				{desc: "25 Wings", price: 19.99}, 
				{desc: "50 Wings", price: 34.99}
			]
		},
		{
			title: "Salad Special",
			price: 18.99,
			desc:"Any Two Salads"
		},
		{
			title: "HoagieSpecial",
			price: 23.99,
			desc: "Two Whole 16” Hoagies and 2 Cans of Soda"
		}
	]
}

######################### Menu #########################

# Pastas
pastas ={
	adds: [
		{title: "Extra Meat", price: 2.50}, 
		{title: "Extra Cheese", price: 2.00}
	],


	pastas: [
		{
			title: "Spaghetti",
			price: 9.95,
			desc: "Spaghetti pasta with marinara sauce and romano cheese"
		},
		{
			title: "Baked Ziti", 
			price: 10.95,
			desc: "Penne pasta with marinara sauce, mozzarella, provolone and romano cheese"
		},
		{
			title: "Cheese Ravioli", 
			price: 10.95,
			desc: "Cheese ravioli with marinara sauce, mozzarella and provolone cheese"
		},
		{
			title: "Beef Ravioli", 
			price: 10.95,
			desc: "Beef ravioli with marinara sauce, mozzarella and provolone cheese"
		},
		{
			title: "Stuffed Shells",
			price: 10.95,
			desc: "Stuffed shells with marinara sauce, mozzarella and provolone cheese"
		},
		{
			title: "Pasta with Meatball", 
			price: 11.95,
			desc: "Spaghetti with meatballs, marinara sauce and romano cheese"
		},
		{
			title: "Pasta with Sausage", 
			price: 12.95,
			desc: "Spaghetti with Italian sausage, marinara sauce and romano cheese"
		},
		{
			title: "Meat Pasta", 
			price: 13.95,
			desc: "Spaghetti with meatballs, Italian sausage, marinara sauce and romano cheese"
		},
		{
			title: "Chicken Parmesan Dinner", 
			price: 13.95,
			desc: "Spaghetti with breaded chicken fillet, marinara sauce, mozzarella and provolone cheese"
		},
		{
			title: "Eggplant Parmesan Dinner", 
			price: 13.95,
			desc: "Spaghetti with breaded eggplant, marinara sauce, mozzarella and provolone cheese"
		},
		{
			title: "Greek Dinner", 
			price: 13.95,
			desc: "Fettuccine with gyro meat, roasted red peppers, black olives, tomato, romano and feta cheese with white sauce"
		},
		{
			title: "Fettuccine Alfredo",
			price: 11.95,
			desc: "Fettuccine pasta with Alfredo sauce and romano cheese"
		},
		{
			title: "Grilled Chicken Alfredo", 
			price: 13.95,
			desc: "Fettuccine pasta with grilled chicken, Alfredo sauce and romano cheese"
		},
		{
			title: "Grilled Chicken and Broccoli Alfredo", 
			price: 14.95,
			desc: "Fettuccine pasta with grilled chicken, fresh broccoli, Alfredo sauce and romano cheese"
		},
		{
			title: "Chicken Carbonara", 
			price: 14.95,
			desc: "Fettuccine pasta with grilled chicken, bacon, mushrooms, Alfredo sauce and romano cheese"
		},
		{
			title: "Pasta with Shrimp", 
			price: 14.95,
			desc: "Fettuccine pasta with sautéed shrimp, Alfredo sauce and romano cheese"
		}
	]
}


# Colzones
colzones = {
	adds: {
		price: [{size: "Small 10”", price: 13.95}, {size: "Large 14”", price: 20.95}],

		title: "Extra Cheese", price: {Small: 1.95, Large: 2.95},
		title: "Vegetables",   price: {Small: 1.95, Large: 2.95},
		title: "Extra Meat",   price: {Small: 1.95, Large: 2.95}
	},
	
	colzones: [
		{
			title: "Five Cheese Calzone",
			desc: "Stuffed with mozzarella, provolone, feta cheese, ricotta cheese, fontinella cheese and tomato sauce"
		},
		{
			title: "Italian Calzone",
			desc: "Stuffed with ham, salami, capicola, pepperoni, mozzarella, provolone and ricotta cheeses and tomato sauce"
		},
		{
			title: "Steak Calzone",
			desc: "Stuffed with steak, mushroom, red onion, green pepper, mozzarella, provolone and ricotta cheeses and tomato sauce"
		},
		{
			title: "Chicken Calzone",
			desc: "Stuffed with grilled chicken strips, mozzarella, provolone and ricotta cheeses and tomato sauce"
		},
		{
			title: "Pepperoni Calzone",
			desc: "Stuffed with pepperoni, mozzarella, provolone and ricotta cheeses and tomato sauce"
		},
		{
			title: "Sausage Calzone",
			desc: "Stuffed with sausage, mozzarella, provolone and ricotta cheeses and tomato sauce"
		},
		{
			title: "Meatball Calzone",
			desc: "Stuffed with meatballs, mozzarella, provolone and ricotta cheeses and tomato sauce"
		},
		{
			title: "Vegetable Calzone",
			desc: "Stuffed with spinach, mushroom, green pepper, red onion, broccoli, fresh garlic, mozzarella, provolone and ricotta cheeses and tomato sauce"
		},
		{
			title: "Seafood Calzone",
			desc: "Stuffed with shrimp, crab meat, mushrooms, red onions, mozzarella, provolone and ricotta cheeses and white sauce"
		},
		{
			title: "Gyro Calzone",
			desc: "Stuffed with gyro meat, onions, mozzarella, provolone, ricotta and feta cheeses and white sauce"
		}
	]
}


# Wedgies
wedgies = {
	adds: {
		price: [{size: "Small 10”", price: 13.95}, {size: "Large14”", price: 20.95}],

		title: "Extra Cheese", price: {Small: 1.95, Large: 2.95},
		title: "Vegetables",   price: {Small: 1.95, Large: 2.95},
		title: "Extra Meat",   price: {Small: 1.95, Large: 2.95}
	},

	wedgies:[
		{
			title: "Italian Wedgie",
			desc: "Stuffed with ham, salami, capicola, pepperoni, mozzarella and provolone cheeses, lettuce, tomato and red onion"
		},
		{
			title: "Steak Wedgie",
			desc: "Stuffed with steak, mushroom, green pepper, mozzarella and provolone cheeses, lettuce, tomato and red onion"
		},
		{
			title: "Chicken Wedgie",
			desc: "Stuffed with grilled chicken strips, mozzarella and provolone cheeses, lettuce, tomato and red onion"
		},
		{
			title: "Gyro Wedgie",
			desc: "Stuffed with gyro meat, mozzarella and provolone cheeses, lettuce, tomato and red onion"
		},
		{
			title: "Turkey Wedgie",
			desc: "Stuffed with turkey, mozzarella and provolone cheeses, lettuce, tomato and red onion"	
		},
		{
			title: "Vegetable Wedgie",
			desc: "Stuffed with spinach, mushrooms, green pepper, red onion, lettuce, tomato, broccoli, mozzarella and provolone cheeses"
		},
		{
			title: "Seafood Wedgie",
			desc: "Stuffed with shrimp, crab meat, mushrooms, mozzarella and provolone cheeses, lettuce, tomatoes and red onions"
		},
		{
			title: "Taco Wedgie",
			desc: "Stuffed with spicy seasoned ground beef, mozzarella, provolone and cheddar cheeses, lettuce, tomatoes, red onions and black olives"
		}
	]
}


# Desserts
dessers = {
	dessers: [
		{
			title: "Oreo Cheesecake", 
			price: 3.95
		},
		{
			title: "New York Cheesecake", 
			price: 3.95
		},
		{
			title: "Strawberry Cheesecake", 
			price: 3.95
		},
		{
			title: "Tiramisu",
			price: 3.95
		},
		{
			title: "Baklava", 
			price: 5.00
		}
	]
}

# Beverages
beverages = {
	beverages:[
		{title: "Cans", price: 1.15},
		{title: "20 oz.", price: 1.85},
		{title: "2 Liter", price: 2.95},
		{title: "20 oz. Water", price: 1.15},
		{title: "Getorade", price: 1.85}
	]
}




# Appetizers

appetizers = {
	appetizers: [
		{
			title: "French Fries"
		}
	]
}










