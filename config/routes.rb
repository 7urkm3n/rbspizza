Rails.application.routes.draw do

  devise_for :users, path: ''

  # SubDomain - Admin
  constraints subdomain: 'admin' do
    get '/' => 'admin/home#index'
    
    namespace :admin, path: '/' do
      resources :products, :path => "promotions", as: "promotions"
      resources :applicants
    end

    # get '/users' => 'admin/admin#users'
    # patch 'users/:id' => 'admin/users_admin#update', as: :user
  end
  
  # V1 Default Website
  # namespace :v1, path: '' do
  #   resources :products, only: [:index, :show], :path => "promotion", as: "promotion" do 
  #     post "applicant_apply" => 'products#applicant_apply', as: :applicants
  #   end
  #   get '/menu-v1' => "home#menu"
  # end 

  # V2 New Design Responsive
  namespace :v2, path: '' do
    get '/menu'     => "home#menu"
    get '/specials' => "home#specials"
    get '/about'    => "home#about_us"
    get '/contact'  => "home#contact"
    get '/order'    => "home#order"
    resources :contacts, only: [:create]
  end

  resources :promotions, only: [:index, :show], :path => "promotion", as: "promotion" do 
    post "applicant_apply" => 'promotions#applicant_apply', as: :applicants
  end

  # root 'products#index'
  root 'v2/home#index'
end
