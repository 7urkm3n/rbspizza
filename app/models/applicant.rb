class Applicant < ApplicationRecord
	belongs_to :product

	def  full_name
	   "#{first_name} #{last_name}"
	end

	def  full_address
		address = "#{address_line}, #{city}, #{state}. #{zip_code}"
   		address = "#{address_line}, #{address_line_two}, #{city}, #{state}. #{zip_code}" if address_line_two.length >= 1
	   	return address
	end
end
