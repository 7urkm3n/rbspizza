class PromotionsController < ApplicationController
  layout "promotion"
  before_action :set_product, only: :show
  before_action :activated, only: :show

  def index
    products = Product.where(active: true)
    @first_4  = products.first(4)
    @last_all = products.offset(4)
  end

  def show
  end

  def applicant_apply
    applied_user = Applicant.where(email: params[:applicant][:email])

    @applicant = Applicant.new(applicant_params)
    @applicant.state = "PA"
    @applicant.product_id = params[:promotion_id].to_i

    respond_to do |format|
       # && applied_user.product_id != params[:promotion_id].to_i
      if (applied_user.length < 2) && @applicant.save
        full_name = "#{@applicant.first_name} #{@applicant.last_name}"
        email     = @applicant.email
        promotion = @applicant.product.title

        ApplicantMailer.no_reply_email(full_name, email, promotion).deliver
        
        # @data = {:status => 200, :message => "Successfully Payback Accepted !"}
        format.js {render 'applicant_apply', locals: {:status => 200, :message => "Successfully Payback Accepted !"}}
      else
        format.js {render 'applicant_apply', locals: {:status => 401, :message => "Unfortunately, Only two Promotions available for each applicant!"}}
      end
    end
  end


private
  def set_product
    @product = Product.find(params[:id])
  end

  def applicant_params
    params.require(:applicant).permit(:first_name, :last_name, :email, :address_line, :address_line_two, :city, :state, :zip_code, :phone_number)
  end

  def activated
    unless @product.active
      redirect_to v1_promotion_index_path
    end
  end
end
