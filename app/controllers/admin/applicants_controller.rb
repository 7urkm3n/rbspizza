class Admin::ApplicantsController < Admin::AdminController
  before_action :set_applicant, only: [:show, :edit, :update, :destroy]

  def index
    @applicants = Applicant.all
  end

  def show
      if params[:destroy]
        destroy
      end
  end

  def new
    @applicant = Applicant.new
  end

  def edit
  end

  def create
    @applicant = Applicant.new(applicant_params)
    @applicant.state = "PA"
    @applicant.product_id = params[:product_id].to_i if params[:product_id]

    respond_to do |format|
      if @applicant.save
        format.html { redirect_to admin_applicants_path, notice: 'Applicant was successfully updated.' }
        format.json { render :show, status: :ok, location: @applicant }
        # format.json { render json: @applicant, status: 200}
      else
        format.html { render :new }
        format.json { render json: @applicant.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @applicant.update(applicant_params)
        format.html { redirect_to admin_applicants_path, notice: 'Applicant was successfully updated.' }
        format.json { render :show, status: :ok, location: @applicant }
      else
        format.html { render :edit }
        format.json { render json: @applicant.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @applicant.destroy
    respond_to do |format|
      format.html { redirect_to admin_applicants_path, notice: 'Applicant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_applicant
      @applicant = Applicant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def applicant_params
      params.require(:applicant).permit(:first_name, :last_name, :email, :address_line, :address_line_two, :city, :state, :zip_code, :phone_number)
    end
end
