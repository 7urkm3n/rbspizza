class Admin::AdminController < ApplicationController
	layout 'admin'
	before_action :authenticate_user!
	# before_action :only_admin

private

	def only_admin
		#use Devise's method
		authenticate_user!

		#custom
		unless current_user.admin
			flash[:alert] = "Your duck is still pending. Please contact support for support."
			redirect_to root_path
		end
	end

end