class V2::ContactsController < V2::V2Controller
  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  # def index
  #   @contacts = Contact.all
  # end

  # def show
  # end

  # def new
  #   @contact = Contact.new
  # end

  # def edit
  # end

  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        
        name = params[:contact][:name]
        email = params[:contact][:email]
        message = params[:contact][:message]
        ContactMailer.contact_email(name, email, message).deliver


        format.html { redirect_to :back, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
        format.js   {}
      else
        format.html { render :back }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
        format.js   {}
      end
    end
  end

  # def update
  #   respond_to do |format|
  #     if @contact.update(contact_params)
  #       format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @contact }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @contact.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # def destroy
  #   @contact.destroy
  #   respond_to do |format|
  #     format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    # def set_contact
    #   @contact = Contact.find(params[:id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:name, :email, :message)
    end
end
