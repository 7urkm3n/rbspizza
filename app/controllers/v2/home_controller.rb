class V2::HomeController < V2::V2Controller
	
  def index
  end

  def menu
  end

  def specials
  end

  def about_us
  end

  def contact
  end
  
  def order
  end
end
