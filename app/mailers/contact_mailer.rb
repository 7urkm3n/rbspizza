class ContactMailer < ApplicationMailer
	default to: "rbspizza@gmail.com"

	def contact_email(name, email, message)
		@name = name
		@email = email
		@content = message

		mail(from: email, subject:"Contact Form Message")
	end
end