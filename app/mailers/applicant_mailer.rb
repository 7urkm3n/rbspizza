class ApplicantMailer < ApplicationMailer
	default from: "rbspizza@gmail.com"

	def no_reply_email(full_name, email, promotion)
		@full_name = full_name
		@email     = email
		@promotion = promotion

		mail( :to => email, :subject => 'no reply - Thanks for applying' )
		
		# mail(from: email, subject:"Confimed: no reply")
	end
end