json.extract! product, :id, :title, :desc, :quantity, :created_at, :updated_at
json.url product_url(product, format: :json)