module ApplicationHelper

	def block(&block)
	end

	def active_class(link_path)
		current_page?(link_path) ? "active" : ""
	end

	def current_page_v2(path)
		"current-menu-item current_page_item" if current_page?(path)
	end
	
end
